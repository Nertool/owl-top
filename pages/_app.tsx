import '../styles/globals.css';
import {AppProps} from 'next/dist/shared/lib/router/router';
import Head from 'next/head';
import React from 'react';
import ym from 'react-yandex-metrika';
import { YMInitializer } from 'react-yandex-metrika';
import {Router} from 'next/router';

function MyApp({Component, pageProps}: AppProps): JSX.Element {
	Router.events.on('routeChangeComplete', (url: string) => {
		if (typeof window !== 'undefined') {
			ym('hit', url);
		}
	});

	return (
		<>
			<Head>
				<title>OWL Top</title>
				<meta name="description" content="App for my portfolio"/>
				<link rel="icon" href={'/favicon.ico'}/>
				<link rel="preconnect" href="https://fonts.googleapis.com"/>
				<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin='true'/>
				<link rel="preconnect" href='https://mc.yandex.ru'/>
				<link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap" rel="stylesheet"/>
				<meta property='og:locale' content='ru_RU' />
			</Head>
			<YMInitializer accounts={[]} options={{webvisor: true, defer: true}} version="2" />
			<Component {...pageProps} />
		</>
	);
}

export default MyApp;
