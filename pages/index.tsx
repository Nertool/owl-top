import {Button, Htag, Input, P, Rating, Tag, Textarea} from '../components';
import {useState} from 'react';
import { withLayout } from '../layout/Layout';
import {GetStaticProps} from 'next';
import axios from 'axios';
import {MenuItem} from '../interfaces/menu.interface';
import {API} from '../helpers/api';

function Home(): JSX.Element {
	const [rating, setRating] = useState<number>(3);

	return (
		<>
			<Htag tag='h1'>Text</Htag>

			<Button appearance='primary' arrow={'right'}>Button</Button>
			<Button appearance='ghost' arrow={'right'}>Button</Button>

			<P size='s'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, saepe.</P>
			<P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, saepe.</P>
			<P size='l'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, saepe.</P>

			<Tag color={'ghost'}>ghost</Tag>
			<Tag color={'red'} size={'m'}>red</Tag>
			<Tag color={'gray'} size={'m'}>gray</Tag>
			<Tag color={'green'} size={'m'} href={'#'}>green</Tag>
			<Tag color={'primary'} size={'m'} href={'#'}>primary</Tag>

			<Rating rating={rating} setRating={setRating} />

			<Input placeholder={'text'} />

			<Textarea placeholder={'test area'} />
		</>
	);
}

export const getStaticProps: GetStaticProps = async () => {
	const firstCategory = 0;
	const {data: menu} = await axios.post<MenuItem[]>(API.topPage.find, {firstCategory});
	return {
		props: { menu, firstCategory },
	};
};

interface HomeProps extends Record<string, unknown> {
	menu: MenuItem[],
	firstCategory: number
}

export default withLayout(Home);
