import styles from './Menu.module.css';
import cn from 'classnames';
import {useContext, KeyboardEvent} from 'react';
import {AppContext} from '../../context/app.context';
import {FirstLevelMenuItem, PageItem} from '../../interfaces/menu.interface';
import Link from 'next/link';
import {useRouter} from 'next/router';
import { firstLevelMenu } from '../../helpers/helpers';
import {motion} from 'framer-motion';

export const Menu = (): JSX.Element => {
	const {menu, setMenu, firstCategory} = useContext(AppContext);
	const router = useRouter();

	const variants = {
		visible: {
			marginBottom: 20,
			transition: {
				when: 'beforeChildren',
				staggerChildren: 0.1
			}
		},
		hidden: {
			marginBottom: 0
		}
	};

	const variantsChildren = {
		visible: {
			opacity: 1,
			height: 29
		},
		hidden: {
			opacity: 0,
			height: 0
		}
	};

	const openSecondLevel = (secondCategory: string): void => {
		setMenu && setMenu(menu.map((item) => {
			if (item._id.secondCategory === secondCategory) {
				item.isOpened = !item.isOpened;
			}
			return item;
		}));
	};

	const openSecondLevelKey = (key: KeyboardEvent, category: string) => {
		if (key.code === 'Space' || key.code === 'Enter') {
			key.preventDefault();
			openSecondLevel(category);
		}
	};

	const buildFirstLevel = () => {
		return (
			<>
				{firstLevelMenu.map((firstMenu) => (
					<div key={firstMenu.route}>
						<Link href={`/${firstMenu.route}`}>
							<a>
								<div className={cn(styles.firstLevel, {
									[styles.firstLevelActive]: firstMenu.id === firstCategory
								})}>
									{firstMenu.icon}
									<span>{firstMenu.name}</span>
								</div>
							</a>
						</Link>
						{firstMenu.id === firstCategory && buildSecondLevel(firstMenu)}
					</div>
				))}
			</>
		);
	};

	const buildSecondLevel = (menuItem: FirstLevelMenuItem) => {
		return (
			<div className={styles.secondBlock}>
				{menu.map((item) => {
					if (item.pages.map((page) => page.alias).includes(router.asPath.split('/')[2])) {
						item.isOpened = true;
					}
					return (
						<div key={item._id.secondCategory}>
							<div tabIndex={0} onKeyDown={(key: KeyboardEvent) => openSecondLevelKey(key, item._id.secondCategory)} className={styles.secondLevel} onClick={() => openSecondLevel(item._id.secondCategory)}>{item._id.secondCategory}</div>
							<motion.div
								layout
								variants={variants}
								initial={item.isOpened ? 'visible' : 'hidden'}
								animate={item.isOpened ? 'visible' : 'hidden'}
								className={styles.secondLevelBlock}
							>
								{buildThirdLevel(item.pages, menuItem.route, item.isOpened ?? false)}
							</motion.div>
						</div>
					);
				})}
			</div>
		);
	};

	const buildThirdLevel = (pages: PageItem[], route: string, isOpened: boolean) => {
		return (
			pages.map((page) => (
				<motion.div key={page._id} variants={variantsChildren}>
					<Link href={`/${route}/${page.alias}`}>
						<a tabIndex={isOpened ? 0 : -1} className={cn(styles.thirdLevel, {
							[styles.thirdLevelActive]: `/${route}/${page.alias}` === router.asPath
						})}>{page.category}</a>
					</Link>
				</motion.div>
			))
		);
	};

	return (
		<div className={styles.menu}>
			{buildFirstLevel()}
		</div>
	);
};
