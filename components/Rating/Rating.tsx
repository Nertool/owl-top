import StarIcon from './star.svg';
import {RatingProps} from './Rating.props';
import {useEffect, useState, KeyboardEvent, forwardRef, ForwardedRef} from 'react';
import cn from 'classnames';
import styles from './Rating.module.css';

export const Rating = forwardRef(({isEditable = false, rating, setRating, error, className, ...props}: RatingProps, ref: ForwardedRef<HTMLDivElement>): JSX.Element => {
	const [ratingArray, setRatingArray] = useState<JSX.Element[]>(new Array(5).fill(<></>));

	const constructRating = (currentRating: number) => {
		const updateArray = ratingArray.map((r: JSX.Element, i: number) => {
			return (
				<StarIcon
					className={cn(styles.star, className, {
						[styles.filled]: i < currentRating,
						[styles.editable]: isEditable,
					})}
					onClick={() => clickHandler(i + 1)}
					tabIndex={isEditable ? -1 : 0}
					onKeyDown={(e: KeyboardEvent<SVGAElement>) => !isEditable && handleSpace(i + 1, e)}
				/>
			);
		});
		setRatingArray(updateArray);
	};

	const handleSpace = (i: number, e: KeyboardEvent<SVGAElement>) => {
		if (e.code !== 'Space' || !setRating) {
			return;
		}
		setRating(i);
	};

	const clickHandler = (i: number) => {
		if (!isEditable || !setRating) {
			return;
		}
		setRating(i);
	};

	const changeDisplay = (i: number) => {
		if (!isEditable) {
			return;
		}
		constructRating(i);
	};

	useEffect(() => {
		constructRating(rating);
	}, [rating]);

	return (
		<div {...props} ref={ref} className={cn(styles.wrapper, {
			[styles.error]: error
		})}>
			{ratingArray.map((r, i) => (
				<span
					className={styles.iconWrapper}
					key={i}
					onMouseEnter={() => changeDisplay(i + 1)}
					onMouseLeave={() => changeDisplay(rating)}
				>
					{r}
				</span>
			))}
			{error && <span className={styles.message}>{error.message}</span>}
		</div>
	);
});
