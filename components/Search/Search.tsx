import {SearchProps} from './Search.props';
import styles from './Search.module.css';
import cn from 'classnames';
import {Button, Input} from '..';
import {useState, KeyboardEvent} from 'react';
import GlassIcon from './glass.svg';
import {useRouter} from 'next/router';

export const Search = ({className, ...props}: SearchProps): JSX.Element => {
	const [search, setSearch] = useState<string>('');
	const router = useRouter();

	const goToSearch = () => {
		router.push({
			pathname: '/search',
			query: {
				q: search
			}
		});
	};

	const handleKeyDown = (evt: KeyboardEvent) => {
		if (evt.key === 'Enter') {
			goToSearch();
		}
	};

	return (
		<div className={cn(className, styles.search)} {...props}>
			<Input
				placeholder='Поиск...'
				value={search}
				onChange={(evt) => setSearch(evt.target.value)}
				className={styles.input}
				onKeyDown={handleKeyDown}
			/>
			<Button
				appearance='primary'
				className={styles.button}
				onClick={goToSearch}
			>
				<GlassIcon />
			</Button>
		</div>
	);
};
