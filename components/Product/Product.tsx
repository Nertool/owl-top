import {ProductProps} from './Product.props';
import styles from './Product.module.css';
import {Card} from '../Card/Card';
import {Button, Divider, Rating, Review, ReviewForm, Tag} from '..';
import {declOfNum, priceRu} from '../../helpers/helpers';
import cn from 'classnames';
import {ForwardedRef, forwardRef, useRef, useState} from 'react';
import React from 'react';
import {motion} from 'framer-motion';

export const Product = motion(forwardRef(({product, className, ...props}: ProductProps, ref: ForwardedRef<HTMLDivElement>): JSX.Element => {
	const [isReviewOpened, setIsReviewOpened] = useState<boolean>(false);
	const reviewRef = useRef<HTMLDivElement>(null);

	const variants = {
		visible: {
			opacity: 1,
			height: 'auto'
		},
		hidden: {
			opacity: 0,
			height: 0
		}
	};

	const scrollToReview = () => {
		setIsReviewOpened(true);
		reviewRef.current?.scrollIntoView({
			behavior: 'smooth',
			block: 'start'
		});
	};

	return (
		<div className={className} {...props} ref={ref}>
			<Card className={styles.product}>
				<div className={styles.logo}>
					<img
						src={process.env.NEXT_PUBLIC_DOMAIN + product.image}
						alt={product.title}
						width={70}
						height={70}
					/>
				</div>
				<div className={styles.title}>{product.title}</div>
				<div className={styles.price}>
					{priceRu(product.price)}
					{product.oldPrice && <Tag className={styles.oldPrice} color='green'>{priceRu(product.price - product.oldPrice)}</Tag>}
				</div>
				<div className={styles.credit}>
					{priceRu(product.credit)}<span className={styles.month}>/мес</span>
				</div>
				<div className={styles.rating}><Rating rating={product.reviewAvg ? product.reviewAvg : product.initialRating} /></div>
				<div className={styles.tags}>{product.categories.map((category) => <Tag key={category} className={styles.category} color='ghost'>{category}</Tag>)}</div>
				<div className={styles.priceTitle}>цена</div>
				<div className={styles.creditTitle}>кредит</div>
				<div className={styles.ratingTitle}><a href={'#ref'} onClick={scrollToReview}>{product.reviewCount} {declOfNum(product.reviewCount, ['отзыв', 'отзыва', 'отзывов'])}</a></div>
				<Divider className={styles.hr} />
				<div className={styles.description}>{product.description}</div>
				<div className={styles.feature}>
					{product.characteristics.map((char) => (
						<div key={char.name} className={styles.characteristic}>
							<span className={styles.characteristicName}>{char.name}</span>
							<span className={styles.characteristicDots} />
							<span className={styles.characteristicValue}>{char.value}</span>
						</div>
					))}
				</div>
				<div className={styles.advBlock}>
					{
						product.advantages &&
						<div className={styles.advantages}>
							<div className={styles.advTitle}>Преимущества</div>
							<div>{product.advantages}</div>
						</div>
					}
					{
						product.disadvantages &&
						<div className={styles.disadvantages}>
								<div className={styles.advTitle}>Недостатки</div>
								<div>{product.disadvantages}</div>
						</div>
					}
				</div>
				<Divider className={cn(styles.hr, styles.hr2)} />
				<div className={styles.actions}>
					<Button appearance='primary'>Узнать подробнее</Button>
					<Button
						appearance='ghost'
						arrow={isReviewOpened ? 'down' : 'right'}
						className={styles.reviewBtn}
						onClick={() => setIsReviewOpened(!isReviewOpened)}
					>
						Читать отзывы
					</Button>
				</div>
			</Card>

			<motion.div animate={isReviewOpened ? 'visible' : 'hidden'} variants={variants} initial={'hidden'}>
				<Card ref={reviewRef} color='blue' className={styles.reviews} tabIndex={isReviewOpened ? 0 : -1}>
					{product.reviews.map((review) => (
						<div key={review._id}>
							<Review review={review} />
							<Divider />
						</div>
					))}
					<ReviewForm productId={product._id} />
				</Card>
			</motion.div>
		</div>
	);
}));
