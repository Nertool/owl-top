import styles from './Advantages.module.css';
import { AdvantagesProps } from './Advantages.props';
import {Card} from '../Card/Card';
import CheckIcon from './check.svg';
import {priceRu} from '../../helpers/helpers';

export const Advantages = ({advantages}: AdvantagesProps): JSX.Element => {
	return (
		<>
			{advantages.map((advantage) => (
				<div key={advantage._id} className={styles.advantage}>
					<CheckIcon />
					<div className={styles.title}>{advantage.title}</div>
					<hr className={styles.vline} />
					<div>{advantage.description}</div>
				</div>
			))}
		</>
	);
};
